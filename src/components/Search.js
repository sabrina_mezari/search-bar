import { useState, useEffect } from "react";
import axios from "axios";

import "./Search.css";

function Search() {

    const [datas, setDatas] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");


    useEffect(() => {
       async function getDatas() {
            const result = await axios.get('https://jsonplaceholder.typicode.com/posts')
            setDatas(result.data);
        }
        getDatas()
    }, []);


    function handleSearchTerm(event) {
        event.target.value.length > 2 && setSearchTerm(event.target.value)
    };


    return (
        <div>

            <div className="searchBar">
                <input 
                    type="text" 
                    name="searchBar" 
                    id="searchBar" 
                    placeholder="Rechercher"
                    onChange={handleSearchTerm}
                />
            </div>
            <div className="search_results">
                {datas.filter((value) => (
                    value.title.toLowerCase().includes(searchTerm.toLowerCase())
                )).map((value) => (
                    <div className="search_result" key={value.id}>
                    {value.title}
                </div>
                ))}
                
            </div>
        </div>
    )
}

export default Search;